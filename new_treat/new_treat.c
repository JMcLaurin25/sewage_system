/*
 * C socket server example, handles multiple clients using threads
 * format from http://www.binarytides.com/server-client-example-c-sockets-linux/
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

#include "networking.h"
#include "report.h"
#include "structs.h"
#include "treatment.h"

// the thread function
void *connection_handler(void *);

int main(void)
{
	int sock_id , client_sock , c;
	struct sockaddr_in server , client;

	// Create socket
	sock_id = socket(AF_INET , SOCK_STREAM , 0);
	if (sock_id == -1){
		printf("Could not create socket");
	}

	// Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	//server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_addr.s_addr = inet_addr("10.0.47.1");
	//server.sin_port = htons(4321);
	server.sin_port = htons(1111);

	// Bind
	if (bind(sock_id,(struct sockaddr *)&server , sizeof(server)) < 0){
		// print the error message
		perror("bind failed. Error");
		return 1;
	}

	// Listen
	listen(sock_id , 5);

	//Thread lock for chlorine and aeration checks
	pthread_mutex_t lock;
	if (pthread_mutex_init(&lock, NULL) != 0) {
		perror("Mutex init Failed!!!");
		return 1;
	}

	size_t total_moles = 0;
	size_t total_chlor = 0;
	size_t total_air = 0;
	size_t total_lead = 0;

	// Accept and incoming connection
	printf("Waiting for incoming connections...\n");
	c = sizeof(struct sockaddr_in);
	while((client_sock = accept(sock_id, (struct sockaddr *)&client, (socklen_t*)&c)))	{
		printf("Connection accepted\n");

		struct conn_args connect_args;
		connect_args.client_sock = client_sock;
		connect_args.lock = &lock;
		connect_args.total_moles = &total_moles;
		connect_args.total_chlor = &total_chlor;
		connect_args.total_air = &total_air;
		connect_args.total_lead = &total_lead;

		pthread_t sniffer_thread;

		if (pthread_create( &sniffer_thread , NULL , connection_handler , (void*) &connect_args) < 0){
			perror("could not create thread");
			close(client_sock);
			return 1;
		}

		// Now join the thread , so that we dont terminate before the thread
		pthread_join(sniffer_thread , NULL);
	}

	if (client_sock < 0){
		perror("accept failed");
		return 1;
	}

	return 0;
}


/*
 * This will handle connection for each client
 * */
void *connection_handler(void *connect_args)
{
	struct conn_args *conn_args = (struct conn_args *)connect_args;
	print_conn_args(conn_args);

	// Get the socket descriptor
	int sock = conn_args->client_sock;
	//int sock = *(int*)sock_id;

	// Receive data here --------------------------------------------------
	// GET HEADER DATA
	struct header head;
	ssize_t recv_sz = read(sock, &head, sizeof(head));
	if (recv_sz < 0) {
		perror("Could not read header");
		close(sock);
		goto done;
	}

	if ((size_t)recv_sz < sizeof(head)) {
		// Send report
		perror("Report sent. Error reading Header");
		const char *data_error = "Not enough data";
		if (report(sock, 8, data_error, strlen(data_error)) < 0) {
			perror("Could not send report");
			goto done;
		}

		fprintf(stderr, "Did not receive a full header (%zd/%zu)\n", recv_sz, sizeof(head));
		close(sock);
		goto done;
	}

	recv_sz = 0;
	
	int payload_type = ntohs(head.type);

	printf("I got header data!\n");

	printf("\ttype:-%d-\n", payload_type);
	printf("\tsize:-%d-\n", ntohs(head.size));

	// PAYLOAD TYPE DECLARED IN HEADER
	size_t bytes_to_read = ntohs(head.size) - sizeof(head);
	struct molecule *payload = calloc(bytes_to_read, 1);
	if (!payload) {
		perror("Failure to calloc");
		goto done;
	}

	// Read payload
	recv(sock, (void *)payload, bytes_to_read, 0);
	//Filter here
	int found_item = 0;
	found_item = filter(&payload, &bytes_to_read, head, conn_args);

	const char *destination = "10.0.47.151";
	//const char *destination = "127.0.0.1";
	char dest_port[4];
	head.type = (uint16_t)htons(0);
	head.size = (uint16_t)htons(sizeof(head) + bytes_to_read);
	strncpy(dest_port, "1111", sizeof(dest_port));

	if (found_item == 2222) { // Trash and large debris
		// Set header size
		head.type = (uint16_t)htons(1);
		strncpy(dest_port, "2222", sizeof("2222"));
	}

	if (bytes_to_read > 0) {
		printf("Main: Sending downstream\n");
		print_payload(payload, bytes_to_read/ 8);

		// If nothing needs to be done send back out to Downstream
		int out_water = connect_outgoing(destination, dest_port);
		if (out_water < 0) {
			fprintf(stderr, "Could not connect from main\n");
			goto done;
		}

		//Combine payload data-----------------------------------------
		pthread_mutex_lock(conn_args->lock);

		size_t payload_sz = sizeof(head) + bytes_to_read;
		char *comb_payload = calloc(payload_sz, 1);
		if (!comb_payload) {
			perror("Error allocating payload memory");
			goto done;
		}
		memcpy(comb_payload, &head, sizeof(head));
		memcpy(comb_payload + sizeof(head), payload, bytes_to_read);

		printf("From main: Sending payload size: %lu\n", bytes_to_read);
		write(out_water, comb_payload, payload_sz);

		free(comb_payload);

		pthread_mutex_unlock(conn_args->lock);
		//-------------------------------------------------------------

		close(out_water);
	} else {
		printf("Nothing sent\n");
	}

done:
	//free(payload);
	close(sock);

	// --------------------------------------------------END DATA RECEPTION

	return 0;
}


