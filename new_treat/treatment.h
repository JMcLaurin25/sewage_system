#ifndef TREATMENT_H
#define TREATMENT_H

#include <unistd.h>
#include "structs.h"

int filter(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args * connect_args);
void print_payload(struct molecule *payload, size_t moles);
void print_conn_args(struct conn_args *conn_args);

#endif
