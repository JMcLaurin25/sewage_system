#include <arpa/inet.h>
#include <libscrypt.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "networking.h"
#include "report.h"
#include "structs.h"
#include "treatment.h"

#define BUF_SZ 64

int treat_fungus(struct molecule *payload, size_t *bytes_to_read, struct conn_args *args, struct header head);
int treat_lead(struct molecule *payload, size_t *bytes_to_read, struct header head, struct conn_args *args);
int treat_debris(struct molecule **payload, size_t *bytes_to_read, struct conn_args *args);
int compact_debris_lead(struct molecule *payload, size_t *bytes_to_read, struct conn_args *conn_args, struct header head);
int treat_mercury(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args *args);
int treat_selenium(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args *args);
int treat_feces(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args *args);
int treat_ammonia(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args *args);
int treat_phosphates(struct molecule *payload, size_t *bytes_to_read);
int treat_chlorine(struct molecule *payload, size_t *bytes_to_read, struct conn_args *args);
int idx_adj(struct molecule *payload, size_t *bytes_to_read, size_t idx_num);

void print_payload(struct molecule *payload, size_t moles)
{
	printf("Num Molecules:");
	printf(" %lu---------------------\n", moles);
	for (size_t idx = 0; idx < moles; idx++) {
		printf("-data: %u ", ntohl(payload[idx].data));
		printf("\tleft: 0x%u ", ntohs(payload[idx].left));
		printf("\tright: 0x%u\n", ntohs(payload[idx].right));
	}
}

size_t air_levels(struct conn_args *args)
{
	if (!args) {
		return 0;
	}
	size_t percent = ((double)*args->total_air / *args->total_moles * 100);
	printf("\n\n>> Air sent: %lu", *args->total_air);
	printf("\tmoles sent: %lu", *args->total_moles);
	printf("\tAir percentage: %lu\n\n", percent);
	size_t moles_to_send = 0;

	if (percent < 1) {
		//Determine how many air molecules to send
		while (percent < 1) {
			moles_to_send += 1;
			*args->total_moles += 1;
			*args->total_air += 1;

			percent = ((double)*args->total_air / *args->total_moles * 100);
			if (percent > 7) {
				break;
			}
		}
	} else {
		return 0;
	}

	return moles_to_send;
}

void print_conn_args(struct conn_args *conn_args)
{
	if (!conn_args) {
		return;
	}

	pthread_mutex_lock(conn_args->lock);
	printf(">> Sock_id:%d", conn_args->client_sock);
	printf("\tTotal Moles:%lu", *conn_args->total_moles);
	printf("\tTotal Chlorine:%lu", *conn_args->total_chlor);
	printf("\tTotal Air:%lu", *conn_args->total_air);
	printf("\tTotal Lead:%lu\n", *conn_args->total_lead);
	pthread_mutex_unlock(conn_args->lock);
}

static bool is_prime(size_t num)
{
	if (num % 2 == 0) { //Even
		return false; // Not Prime
	}

	size_t num_root = sqrt(num);

	for (size_t idx_p = 3; idx_p < num_root; idx_p += 2) {
		if (num % idx_p == 0) {
			return false; // Not Prime
		}
	}
	return true;
}

static bool is_undul(size_t num)
{
	// Undulation modified from Asamuels 
	if (num == 0) {
		return false;
	} else if (num <= 9) {
		// single numbers are undulating
		return true;
	}

	char str_num[10] = {'\0'};
	int prev = 0;
	int temp = 0;

	// Convert number to string
	snprintf(str_num, sizeof(str_num),"%d", (int)num);
	size_t number_len = strlen(str_num) - 1;

	// Set first number
	if(str_num[0] > str_num[1]) {
		prev = 1;
	} else if (str_num[0] < str_num[1]) {
		prev = -1;
	} else {
		return false;
	}

	for(size_t idx = 1; idx < number_len; idx++) {
		if(str_num[idx] == '\0') {
			continue;
		}

		temp = str_num[idx] - str_num[idx+1];
		if(temp > 0) {
			temp = 1;
			if(prev == temp) {
				// not undulating
				return false;
			}
			prev = temp;
		} else if(temp < 0) {
			temp = -1;
			if(prev == temp) {
				// not undulating
				return false;
			}
			prev = temp;
		} else {
			return false;
		}
	}

	return true;
}

static bool is_triangular(size_t num)
{
	// An integer x is triangular if and only if 8x + 1 is a square.
	// Triangular roots and tests for triangular numbers:
	// https://en.wikipedia.org/wiki/Triangular_number
	return (fmod((0.5 * (sqrt(8 * num + 1)) - 0.5), 1) < 1e-3) ? true : false;
}

// Fibonacci check source from ASamuels
static bool is_fib(uint32_t data)
{
	switch(data)
	{
		case 1:
		case 2:
		case 3:
		case 5:
		case 8:
		case 13:
		case 21:
		case 34:
		case 55:
		case 89:
		case 144:
		case 233:
		case 377:
		case 610:
		case 987:
		case 1597:
		case 2584:
		case 4181:
		case 6765:
		case 10946:
		case 17711:
		case 28657:
		case 46368:
		case 75025:
		case 121393:
		case 196418:
		case 317811:
		case 514229:
		case 832040:
		case 1346269:
		case 2178309:
		case 3524578:
		case 5702887:
		case 9227465:
		case 14930352:
		case 24157817:
		case 39088169:
		case 63245986:
		case 102334155:
		case 165580141:
		case 267914296:
		case 433494437:
		case 701408733:
		case 1134903170:
		case 1836311903:
		case 2971215073:
			return true;

		default:
			return false;
	}

	return false;
}

static size_t _inc_mole(struct conn_args *args)
{
	if (!args) {
		return 1;
	}

	pthread_mutex_lock(args->lock);
	*args->total_moles += 1 ;
	pthread_mutex_unlock(args->lock);
	return 0;
}
static size_t _dec_mole(struct conn_args *args)
{
	if (!args) {
		return 1;
	}

	pthread_mutex_lock(args->lock);
	*args->total_moles -= 1;
	pthread_mutex_unlock(args->lock);
	return 0;
}

static size_t _inc_chlor(struct conn_args *args)
{
	if (!args) {
		return 1;
	}

	pthread_mutex_lock(args->lock);
	*args->total_chlor += 1;
	pthread_mutex_unlock(args->lock);
	return 0;
}
static size_t _dec_chlor(struct conn_args *args)
{
	if (!args) {
		return 1;
	}

	pthread_mutex_lock(args->lock);
	*args->total_chlor -= 1;
	pthread_mutex_unlock(args->lock);
	return 0;
}

static size_t _inc_air(struct conn_args *args)
{
	if (!args) {
		return 1;
	}

	pthread_mutex_lock(args->lock);
	*args->total_air += 1;
	pthread_mutex_unlock(args->lock);
	return 0;
}

static size_t _inc_lead(struct conn_args *args)
{
	if (!args) {
		return 1;
	}

	pthread_mutex_lock(args->lock);
	*args->total_lead += 1;
	pthread_mutex_unlock(args->lock);
	return 0;
}

int filter(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args * conn_args)
{
	// Find Fungus
	treat_fungus(*payload, bytes_to_read, conn_args, head);
	print_payload(*payload, *bytes_to_read / 8);
	print_conn_args(conn_args);

	// Find debris
	if (treat_debris(payload, bytes_to_read, conn_args) == 2222) {
		compact_debris_lead(*payload, bytes_to_read, conn_args, head);
		printf("--- Payload after Debris ---\n");
		print_payload(*payload, *bytes_to_read / 8);
		print_conn_args(conn_args);
		printf("Returning 2222\n");
		return 2222;
	}
	printf("--- Payload after Debris ---\n");
	print_payload(*payload, *bytes_to_read / 8);
	print_conn_args(conn_args);

	// Find lead
	printf("Treating Lead\n");
	if(treat_lead(*payload, bytes_to_read, head, conn_args) == 0) {
		// Lead found in Residential

	}
	print_payload(*payload, *bytes_to_read / 8);

	// Find mercury
	if (*bytes_to_read > 0) {
		if (treat_mercury(payload, bytes_to_read, head, conn_args) == 0) {
			const char *merc_illegal_dump = "Illegal Mercury Dumping in Residential";
			size_t ill_merc_msg_sz = strlen(merc_illegal_dump);

			// Mercury found in Residential
			report(conn_args->client_sock, 8, merc_illegal_dump, ill_merc_msg_sz);
		}
		printf("--- Payload after Mercury ---\n");
		print_payload(*payload, *bytes_to_read / 8);
		print_conn_args(conn_args);
		printf("Bytes_2_read: %lu\n", *bytes_to_read);
	}

	// Find selenium
	if (*bytes_to_read > 0) {
		treat_selenium(payload, bytes_to_read, head, conn_args);
		/*
		   if (treat_selenium(payload, bytes_to_read, head, conn_args) == 0) {
		   const char *sel_illegal_dump = "Illegal Selenium Dumping in Residential";
		   size_t ill_sel_msg_sz = strlen(sel_illegal_dump);

		// Selenium found in Residential
		report(conn_args->client_sock, 8, sel_illegal_dump, ill_sel_msg_sz);
		}
		*/
		printf("--- Payload after Selenium ---\n");
		print_payload(*payload, *bytes_to_read / 8);
		print_conn_args(conn_args);
	}

	// Find feces
	if (*bytes_to_read > 0) {
		treat_feces(payload, bytes_to_read, head, conn_args);
		printf("--- Payload after Feces ---\n");
		print_payload(*payload, *bytes_to_read / 8);
		print_conn_args(conn_args);
	}

	// Find ammonia
	if (*bytes_to_read > 0) {
		treat_ammonia(payload, bytes_to_read, head, conn_args);
		printf("--- Payload after Ammonia ---\n");
		print_payload(*payload, *bytes_to_read / 8);
		print_conn_args(conn_args);
	}

	// Find phosphate
	if (*bytes_to_read > 0) {
		treat_phosphates(*payload, bytes_to_read);
		printf("--- Payload after Phosphate ---\n");
		print_payload(*payload, *bytes_to_read / 8);
		print_conn_args(conn_args);
	}

	// Find chlorine
	if (*bytes_to_read > 0) {
		treat_chlorine(*payload, bytes_to_read, conn_args);
		printf("--- Payload after Chlorine ---\n");
		print_payload(*payload, *bytes_to_read / 8);
		print_conn_args(conn_args);
	}
	return 0;
}

/*
 * Treatment methods below Detection above
 */

int treat_fungus(struct molecule *payload, size_t *bytes_to_read, struct conn_args *args, struct header head)
{
	if (!payload) {
		fprintf(stderr, "Payload not found treat selenium\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}

	size_t moles = *bytes_to_read / 8;

	// Test to for lead
	printf("Moles: %lu\n", moles);
	for (size_t idx = 0; idx < moles; idx++) {
		if (is_fib(ntohl(payload[idx].data)) == false) {
			continue;
		}

		// Place hazardous molecule in container
		struct hz_container hazmat = {0};
		hazmat.data = payload[idx].data;
		printf("Hazardous fungus data: %u\n", ntohl(hazmat.data));

		//REPLACE WITH AIR {0}
		payload[idx].data = 0;

		//Send hazmat downstream to 8888  
		int out_water = connect_outgoing("downstream", "8888");
		//TODO: int out_water = connect_outgoing("127.0.0.1", "8888");
		if (out_water < 0) {
			fprintf(stderr, "Could not connect to 8888\n");
		} else {
			printf("Fungus: Sending hazmat to 8888\n");
			head.type = (uint16_t)htons(4);
			head.size = (uint16_t)htons(sizeof(head) + sizeof(hazmat)); // Set header size

			//Combine payload data-----------------------------------------
			pthread_mutex_lock(args->lock);

			size_t payload_sz = sizeof(head) + sizeof(hazmat);
			char *comb_payload = calloc(payload_sz, 1);
			if (!comb_payload) {
				perror("Error allocating payload memory");
				return -3;
			}
			memcpy(comb_payload, &head, sizeof(head));
			memcpy(comb_payload + sizeof(head), &hazmat, sizeof(hazmat));

			write(out_water, comb_payload, payload_sz);

			free(comb_payload);

			pthread_mutex_unlock(args->lock);
			//-------------------------------------------------------------

		}	

		close(out_water);
	}

	return 0;
}




int treat_debris(struct molecule **payload, size_t *bytes_to_read, struct conn_args *conn_args)
{
	if (!payload) {
		fprintf(stderr, "Payload not found in treat_debris\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}

	size_t moles = *bytes_to_read / 8;

	for (size_t idx = 0; idx < moles; idx++) {
		_inc_mole(conn_args);

		// Set invalid indices to FFFF
		if (ntohs(((struct molecule *)*payload)[idx].left) > moles) {
			((struct molecule *)*payload)[idx].left = 0xFFFF;
		}
		if (ntohs(((struct molecule *)*payload)[idx].right) > moles) {
			((struct molecule *)*payload)[idx].right = 0xFFFF; 
		}

		// Remove molecule if data == 0
		if (ntohl(((struct molecule *)*payload)[idx].data) == 0) {
			// Correct Indices
			idx_adj(*payload, bytes_to_read, idx + 1);
			_dec_mole(conn_args);

			// Remove air molecule from payload
			memmove(*payload + idx, *payload + (idx + 1), (moles - (idx + 1)) * 8);
			*bytes_to_read -= 8;
			moles--;
			idx--;
			*payload = realloc(*payload, *bytes_to_read);
		}
	}

	// Now check payload for debris (65535)
	for (size_t idx = 0; idx < moles; idx++) {
		if ((((struct molecule *)*payload)[idx].left == 0xFFFF) || (((struct molecule *)*payload)[idx].right == 0xFFFF)) {

			return 2222;
		}
		if (((struct molecule *)*payload)[idx].data == 0) {
			_inc_air(conn_args);
		}
	}
	return 0;
}

int compact_debris_lead(struct molecule *payload, size_t *bytes_to_read, struct conn_args *args, struct header head)
{
	if (!payload) {
		fprintf(stderr, "Payload not found in treat_mercury\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}

	size_t moles = *bytes_to_read / 8;

	for (size_t idx = 0; idx < moles; idx++) {
		if (is_prime(payload[idx].data) == true) {
			continue; // Is prime, NOT LEAD
		} else if (is_undul(payload[idx].data) == true) {
			continue; // Is undulating, NOT LEAD
		} else if (is_triangular(payload[idx].data) == false) {
			continue; // Not triangular, NOT LEAD
		}

		// Lead found
		_inc_lead(args);

		// Place hazardous molecule in container
		struct hz_container hazmat = {0};
		hazmat.data = payload[idx].data;
		printf("Hazardous data: %u\n", ntohl(hazmat.data));

		//SWAP LEFT AND RIGHT
		uint16_t temp = payload[idx].left;
		payload[idx].left = payload[idx].right;
		payload[idx].right = temp;

		//REPLACE WITH AIR {0}
		payload[idx].data = 0;

		//Send hazmat downstream to 8888  
		int out_water = connect_outgoing("downstream", "8888");
		//TODO: int out_water = connect_outgoing("127.0.0.1", "8888");
		if (out_water < 0) {
			fprintf(stderr, "Could not connect to 8888\n");
		} else {
			printf("TODO: LEAD Send hazmat  to 8888\n");
			head.type = (uint16_t)htons(4);
			head.size = (uint16_t)htons(sizeof(head) + sizeof(hazmat)); // Set header size

			//Combine payload data-----------------------------------------
			pthread_mutex_lock(args->lock);

			size_t payload_sz = sizeof(head) + sizeof(hazmat);
			char *comb_payload = calloc(payload_sz, 1);
			if (!comb_payload) {
				perror("Error allocating payload memory");
				return -3;
			}
			memcpy(comb_payload, &head, sizeof(head));
			memcpy(comb_payload + sizeof(head), &hazmat, sizeof(hazmat));

			write(out_water, comb_payload, payload_sz);

			free(comb_payload);

			pthread_mutex_unlock(args->lock);
			//-------------------------------------------------------------

		}	

		close(out_water);
	}
	return 0;
}

/*
 * Mercury Treatment derived from ASamuels
 */
int treat_mercury(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args *args)
{
	if (!payload) {
		fprintf(stderr, "Payload not found in treat_mercury\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}

	size_t moles = *bytes_to_read / 8;

	for (size_t idx = 0; idx < moles; idx++) {
		if (moles == 1 ) {
			break;
		}
		uint32_t *indegrees = calloc(sizeof(uint32_t), moles + 1);
		uint32_t least_data = 0;
		uint16_t least_data_idx = 0;

		//Count molecule indice
		for(size_t n = 0; n < moles; n++) {
			indegrees[ntohs(((struct molecule *)*payload)[n].left)]++;
			indegrees[ntohs(((struct molecule *)*payload)[n].right)]++;
		}

		// Find least value indicee of zero. Iterate indegree array
		for(size_t n = 1; n <= moles; n++) {
			if (indegrees[n] == 0) {
				if (least_data == 0) {
					least_data = ntohl(((struct molecule *)*payload)[n-1].data);
					least_data_idx = n; // FIX THIS
				} else if (ntohl(((struct molecule *)*payload)[n-1].data) < least_data) {
					least_data = ntohl(((struct molecule *)*payload)[n-1].data);
					least_data_idx = n; // FIX THIS
				}
			}
		}

		// Place hazardous molecule in container
		printf("Mercury found (%u)\n", least_data);
		struct hz_container hazmat = {0};
		if (least_data != 0) {
			hazmat.data = least_data;
		} else {
			// No indegree of zero
			printf("Breaking. No Indegree of zero\n");
			break;
		}

		if (moles > 1) {
			// Correct Indices
			idx_adj(*payload, bytes_to_read, least_data_idx);
			for (size_t val = 0; val < moles; val++) {
				printf("%u:", ntohl(((struct molecule *)*payload)[val].data));
			}
			printf("\n");

			// Remove mercury molecule from payload
			memmove(*payload + least_data_idx - 1, *payload + least_data_idx, (moles - least_data_idx) * 8);
			*bytes_to_read -= 8;
			moles--;
			idx--;
			*payload = realloc(*payload, *bytes_to_read);

			// CHECK AIR PERCENTAGE AND SEND IF NEEDED
			size_t air_moles = air_levels(args);
			struct molecule air = {0};
			printf("AIR Data: %u Left: %u Right: %u\n", air.data, air.left, air.right);
			printf("Air molecules to send: %lu\n", air_moles);

			//Send hazmat downstream to 8888  
			int out_water = connect_outgoing("downstream", "8888");
			//TODO: int out_water = connect_outgoing("127.0.0.1", "8888");
			if (out_water < 0) {
				fprintf(stderr, "Could not connect to 8888\n");
			} else {
				printf("Mercury Send hazmat to 8888\n");
				head.type = (uint16_t)htons(4);
				head.size = (uint16_t)htons(sizeof(head) + sizeof(hazmat) + (air_moles * 8)); // Set header size

				//Combine payload data-----------------------------------------
				pthread_mutex_lock(args->lock);

				size_t payload_sz = sizeof(head) + sizeof(hazmat) + (sizeof(air) * air_moles);
				char *comb_payload = calloc(payload_sz, 1);
				if (!comb_payload) {
					perror("Error allocating payload memory");
					return -3;
				}
				memcpy(comb_payload, &head, sizeof(head));
				memcpy(comb_payload + sizeof(head), &hazmat, sizeof(hazmat));

				size_t offset = sizeof(head) + sizeof(hazmat);
				for (size_t idx = 0; idx < air_moles; idx++) {
					memcpy(comb_payload + offset, &air, sizeof(air));
					offset += sizeof(air);
				}

				write(out_water, comb_payload, payload_sz);

				free(comb_payload);

				pthread_mutex_unlock(args->lock);
				//-------------------------------------------------------------
			}	

			close(out_water);
		}

		printf("Removed Mercury\n");
		free(indegrees);
		print_payload(*payload, *bytes_to_read / 8);
	}

	return 0;
}

int treat_selenium(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args *args)
{
	if (!payload) {
		fprintf(stderr, "Payload not found treat selenium\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}


	size_t moles = *bytes_to_read / 8;
	size_t max_data = 0;
	size_t max_idx = 0;
	size_t idx_found;

	// Test to for selenium
	printf("Moles: %lu\n", moles);
	for (size_t idx = 0; idx < moles; idx++) {
		idx_found = 0;
		if (ntohl(((struct molecule *)*payload)[idx].data) > max_data) {
			max_data = ntohl(((struct molecule *)*payload)[idx].data);
			max_idx = idx + 1;
		}
		for (size_t idy = 0; idy < moles; idy++) {
			if (ntohs(((struct molecule *)*payload)[idy].left) == ntohs(((struct molecule *)*payload)[idy].right)) {
				if (ntohs(((struct molecule *)*payload)[idy].left) == idx + 1) {
					//Find if the current index is present in payload
					printf("Index found\n");
					idx_found = 1;
					break;
				} else {
					continue;
				}
			}
		}
		if (idx_found == 0) {
			printf("Got to return\n");
			//Current index not found in the payload. NOT CIRCULAR
			return 1;
		}
	}

	// If you get here, data is Circular. Treatment for selenium starts

	// Place hazardous molecule in container
	struct hz_container hazmat = {0};
	hazmat.data = ((struct molecule *)*payload)[max_idx].data;
	printf("Hazardous data: %u\n", ntohl(hazmat.data));

	for (size_t idx = 0; idx < moles; idx++) {
		//Find molecule pointing to maximum data molecule
		if (ntohs(((struct molecule *)*payload)[idx].left) == max_idx) {
			((struct molecule *)*payload)[idx].left = 0;
			((struct molecule *)*payload)[idx].right = 0;
		}
	}

	// Correct Indices
	idx_adj(*payload, bytes_to_read, max_idx);

	// Remove selenium from payload
	memmove(*payload + max_idx - 1, *payload + max_idx, (moles - max_idx) * 8);
	*bytes_to_read -= 8;
	*payload = realloc(*payload, *bytes_to_read);

	// CHECK AIR PERCENTAGE AND SEND IF NEEDED
	size_t air_moles = air_levels(args);
	struct molecule air = {0};
	printf("AIR Data: %u Left: %u Right: %u\n", air.data, air.left, air.right);
	printf("Air molecules to send: %lu\n", air_moles);

	//Send hazmat downstream to 8888  
	int out_water = connect_outgoing("downstream", "8888");
	//TODO: int out_water = connect_outgoing("127.0.0.1", "8888");
	if (out_water < 0) {
		fprintf(stderr, "Could not connect to 8888\n");
	} else {
		printf("TODO: Selenium Send hazmat  to 8888\n");
		head.type = (uint16_t)htons(4);
		head.size = (uint16_t)htons(sizeof(head) + sizeof(hazmat) + (air_moles *8)); // Set header size

		//Combine payload data-----------------------------------------
		pthread_mutex_lock(args->lock);

		size_t payload_sz = sizeof(head) + sizeof(hazmat) + (sizeof(air) * air_moles);
		char *comb_payload = calloc(payload_sz, 1);
		if (!comb_payload) {
			perror("Error allocating payload memory");
			return -3;
		}
		memcpy(comb_payload, &head, sizeof(head));
		memcpy(comb_payload + sizeof(head), &hazmat, sizeof(hazmat));

		size_t offset = sizeof(head) + sizeof(hazmat);
		for (size_t idx = 0; idx < air_moles; idx++) {
			memcpy(comb_payload + offset, &air, sizeof(air));
			offset += sizeof(air);
		}

		write(out_water, comb_payload, payload_sz);

		free(comb_payload);

		pthread_mutex_unlock(args->lock);
		//-------------------------------------------------------------
	}	

	close(out_water);

	return 0;
}

static uint8_t *sludgify(uint32_t data)
{
	uint8_t *buf = calloc(sizeof(uint8_t), BUF_SZ);

	char input[16];
	snprintf(input, sizeof(input), "%u", data);
	const char *salt = "I Hate Liam Echlin";

	libscrypt_scrypt((const uint8_t *)input, strlen(input),
			(const uint8_t *)salt, strlen(salt),
			2048, 4, 4,
			(uint8_t *)buf, BUF_SZ);

	return buf;
}

int treat_feces(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args *args)
{
	if (!payload) {
		fprintf(stderr, "Payload not found in treat_feces\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}


	size_t moles = *bytes_to_read / 8;

	for (size_t idx = 0; idx < moles; idx++) {
		// Remove molecule if data == 0
		if (is_prime(ntohl(((struct molecule *)*payload)[idx].data))) {
			//Sludifying
			uint8_t *sludge = sludgify(ntohl(((struct molecule *)*payload)[idx].data));

			// Correct Indices
			idx_adj(*payload, bytes_to_read, idx + 1);

			//Remove feces from molecules
			memmove(*payload + idx, *payload + (idx + 1), (moles - (idx + 1)) * 8);
			*bytes_to_read -= 8;
			moles -= 1;
			idx -= 1;
			*payload = realloc(*payload, *bytes_to_read);

			// CHECK AIR PERCENTAGE AND SEND IF NEEDED
			size_t air_moles = air_levels(args);
			struct molecule air = {0};
			printf("AIR Data: %u Left: %u Right: %u\n", air.data, air.left, air.right);
			printf("Air molecules to send: %lu\n", air_moles);

			//Send feces downstream to 4444
			int out_water = connect_outgoing("downstream", "4444");
			//TODO: int out_water = connect_outgoing("127.0.0.1", "4444");
			if (out_water < 0) {
				fprintf(stderr, "Could not connect to 4444\n");
			} else {
				printf("Sent feces sludge to 4444\n");
				head.type = (uint16_t)htons(2);
				head.size = (uint16_t)htons(sizeof(head) + BUF_SZ + (air_moles * 8)); // Set header size

				//Combine payload data-----------------------------------------
				pthread_mutex_lock(args->lock);

				size_t payload_sz = sizeof(head) + BUF_SZ + (sizeof(air) * air_moles);
				char *comb_payload = calloc(payload_sz, 1);
				if (!comb_payload) {
					perror("Error allocating payload memory");
					return -3;
				}
				memcpy(comb_payload, &head, sizeof(head));
				memcpy(comb_payload + sizeof(head), sludge, BUF_SZ);

				size_t offset = sizeof(head) + BUF_SZ;
				for (size_t idx = 0; idx < air_moles; idx++) {
					memcpy(comb_payload + offset, &air, sizeof(air));
					offset += sizeof(air);
				}

				write(out_water, comb_payload, payload_sz);

				free(comb_payload);

				pthread_mutex_unlock(args->lock);
				//-------------------------------------------------------------
			}	

			close(out_water);
			free(sludge);
		}
	}
	return 0;
}

int treat_ammonia(struct molecule **payload, size_t *bytes_to_read, struct header head, struct conn_args *args)
{
	if (!payload) {
		fprintf(stderr, "Payload not found treat ammonia\n");
		return 1;
	}
	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}

	size_t moles = *bytes_to_read / 8;
	for (size_t idx = 0; idx < moles; idx++) {
		// Remove molecule if data is undulating
		if (is_undul(ntohl(((struct molecule *)*payload)[idx].data))) {
			//Sludifying
			uint8_t *sludge = sludgify(ntohl(((struct molecule *)*payload)[idx].data));
			uint8_t number = ntohl(((struct molecule *)*payload)[idx].data);

			// Correct Indices
			idx_adj(*payload, bytes_to_read, idx + 1);

			//Remove ammonia from molecules
			memmove(*payload + idx, *payload + (idx + 1), (moles - (idx + 1)) * 8);
			*bytes_to_read -= 8;
			moles -= 1;
			idx -= 1;
			*payload = realloc(*payload, *bytes_to_read);

			// CHECK AIR PERCENTAGE AND SEND IF NEEDED
			size_t air_moles = air_levels(args);
			struct molecule air = {0};
			printf("AIR Data: %u Left: %u Right: %u\n", air.data, air.left, air.right);
			printf("Air molecules to send: %lu\n", air_moles);

			//Send feces downstream to 4444
			int out_water = connect_outgoing("downstream", "4444");
			//TODO: int out_water = connect_outgoing("127.0.0.1", "4444");
			if (out_water < 0) {
				fprintf(stderr, "Could not connect to 4444\n");
			} else {
				printf("Sent ammonia(%u) sludge to 4444\n", number);
				head.type = (uint16_t)htons(2);
				head.size = (uint16_t)htons(sizeof(head) + BUF_SZ + (air_moles *8)); // Set header size

				//Combine payload data-----------------------------------------
				pthread_mutex_lock(args->lock);

				size_t payload_sz = sizeof(head) + BUF_SZ + (sizeof(air) * air_moles);
				char *comb_payload = calloc(payload_sz, 1);
				if (!comb_payload) {
					perror("Error allocating payload memory");
					return -3;
				}
				memcpy(comb_payload, &head, sizeof(head));
				memcpy(comb_payload + sizeof(head), sludge, BUF_SZ);

				size_t offset = sizeof(head) + BUF_SZ;
				for (size_t idx = 0; idx < air_moles; idx++) {
					memcpy(comb_payload + offset, &air, sizeof(air));
					offset += sizeof(air);
				}

				write(out_water, comb_payload, payload_sz);

				free(comb_payload);

				pthread_mutex_unlock(args->lock);
				//-------------------------------------------------------------
			}	

			close(out_water);
			free(sludge);
		}
	}
	return 0;
}

int treat_phosphates(struct molecule *payload, size_t *bytes_to_read)
{
	if (!payload) {
		fprintf(stderr, "Payload not found treat_phosphates\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}


	size_t head = 0;
	size_t tail = 0;
	size_t l_idx = 0;
	size_t r_idx = 0;
	size_t endpoint = 0;

	size_t moles = *bytes_to_read / 8;

	for (size_t idx = 0; idx < moles; idx++) {
		l_idx = ntohs(payload[idx].left);
		r_idx = ntohs(payload[idx].right);

		if (((ntohs(payload[l_idx].left) - 1) == (int)idx) || ((ntohs(payload[l_idx].right) - 1) == (int)idx)) {
			continue;
		} else if (((ntohs(payload[r_idx].left) - 1) == (int)idx) || ((ntohs(payload[r_idx].right) - 1) == (int)idx)) {
			continue;
		}

		if (head == 0) {
			head = idx;
		} else {
			tail = idx;
		}

		endpoint++;
	}

	//Determine Sole head
	size_t sole_head = 0;

	if (endpoint == 2) {
		printf("This is phosphate\n");
		if (ntohl(payload[head].data) > ntohl(payload[tail].data)) {
			sole_head = head;
		} else {
			sole_head = tail;
		}
	}

	//Correct new Solehead
	if (ntohs(payload[sole_head].left) == 0) {
		payload[sole_head].left = payload[sole_head].right;
	} else {
		payload[sole_head].right = payload[sole_head].left;
	}

	return 0;
}

int treat_chlorine(struct molecule *payload, size_t *bytes_to_read, struct conn_args *args)
{
	if (!payload) {
		fprintf(stderr, "Payload not found treat_chlorine\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}

	size_t moles = *bytes_to_read / 8;

	// Check to verify chlorine. Chain linked list.
	for (size_t idx = 0; idx < moles; idx++) {
		if (ntohs(payload[idx].left) == ntohs(payload[idx].right)) {
			_inc_chlor(args);
			continue;
		} else {
			// Not a Chain linked list
			return 1;
		}
	}

	// Treatment for chlorine. Convert to single linked list.
	for (size_t idx = 0; idx < moles; idx++) {
		//Check chlorine levels
		if ((((double)*args->total_chlor / *args->total_air) * 100) > 5) {
			// Remove right link
			payload[idx].right = 0;
			_dec_chlor(args);
		}
	}

	return 0;
}

int treat_lead(struct molecule *payload, size_t *bytes_to_read, struct header head, struct conn_args *args)
{
	if (!payload) {
		fprintf(stderr, "Payload not found treat selenium\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}

	size_t moles = *bytes_to_read / 8;

	// Test to for lead
	printf("Moles: %lu\n", moles);
	for (size_t idx = 0; idx < moles; idx++) {
		if (is_prime(ntohl(payload[idx].data)) == true) {
			continue; // Is prime, NOT LEAD
		} else if (is_undul(ntohl(payload[idx].data)) == true) {
			continue; // Is undulating, NOT LEAD
		} else if (is_triangular(ntohl(payload[idx].data)) == false) {
			continue; // Not triangular, NOT LEAD
		}

		// Lead found
		_inc_lead(args);

		// Place hazardous molecule in container
		struct hz_container hazmat = {0};
		hazmat.data = payload[idx].data;
		printf("Hazardous data: %u\n", ntohl(hazmat.data));

		//REPLACE WITH AIR {0}
		payload[idx].data = 0;

		//Send hazmat downstream to 8888  
		int out_water = connect_outgoing("downstream", "8888");
		//TODO: int out_water = connect_outgoing("127.0.0.1", "8888");
		if (out_water < 0) {
			fprintf(stderr, "Could not connect to 8888\n");
		} else {
			printf("TODO: LEAD Send hazmat  to 8888\n");
			head.type = (uint16_t)htons(4);
			head.size = (uint16_t)htons(sizeof(head) + sizeof(hazmat)); // Set header size

			//Combine payload data-----------------------------------------
			pthread_mutex_lock(args->lock);

			size_t payload_sz = sizeof(head) + sizeof(hazmat);
			char *comb_payload = calloc(payload_sz, 1);
			if (!comb_payload) {
				perror("Error allocating payload memory");
				return -3;
			}
			memcpy(comb_payload, &head, sizeof(head));
			memcpy(comb_payload + sizeof(head), &hazmat, sizeof(hazmat));

			write(out_water, comb_payload, payload_sz);

			free(comb_payload);

			pthread_mutex_unlock(args->lock);
			//-------------------------------------------------------------

		}	

		close(out_water);
	}
	return 0;
}

int idx_adj(struct molecule *payload, size_t *bytes_to_read, size_t idx_num)
{
	if (!payload) {
		fprintf(stderr, "Payload not found in idx_adj\n");
		return 1;
	}

	if (!bytes_to_read) {
		perror("bytes_to_read not found");
		return 1;
	} else if (*bytes_to_read <= 0) {
		return 1;
	}

	size_t moles = *bytes_to_read / 8;

	// Correct pointers that are pointing to the removed molecule
	for (size_t idx = 0; idx < moles; idx++) {
		if (ntohs(payload[idx].left) == idx_num) {
			payload[idx].left = payload[idx_num].left;
		}
		if (ntohs(payload[idx].right) == idx_num) {
			payload[idx].right = payload[idx_num].right;
		}
	}

	for (size_t idx = 0; idx < moles; idx++) {
		if (ntohs(payload[idx].left) == idx_num) {
			payload[idx].left = 0; 
		}
		if (ntohs(payload[idx].right) == idx_num) {
			payload[idx].right = 0;
		}

		if (ntohs(payload[idx].left) > idx_num && ntohs(payload[idx].left) != 0xFFFF) {
			payload[idx].left -= htons(1);
		}
		if (ntohs(payload[idx].right) > idx_num && ntohs(payload[idx].right != 0xFFFF)) {
			payload[idx].right -= htons(1);
		}
	}
	return 0;
}

