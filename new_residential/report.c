
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include <arpa/inet.h>

#include "networking.h"
#include "report.h"
#include "structs.h"

int report(int sock_id, size_t err_type, const char *message, size_t msg_len)
{
	if (!message) {
		perror("Error creating report");
		return -1;
	}

	struct report report_err = {0};
	struct header head = {0};

	head.type = (uint16_t)htons(8); // Reporting type
	head.size = (uint16_t)htons(sizeof(head) + sizeof(report_err)); // Reporting type

	report_err.error_type = (uint16_t)htons(err_type);
	strncpy(report_err.message, message, msg_len);

	// Incomplete data. Send report
	socklen_t socklength;
	struct sockaddr_storage peer_addr;

	int error_report = connect_outgoing("downstream", "9999");
	//int error_report = connect_outgoing("127.0.0.1", "9999");
	if(error_report < 0) {
		perror("Could not connect downstream");
		return -2;
	}

	size_t payload_sz = sizeof(head) + sizeof(report_err);
	char *comb_payload = calloc(payload_sz, 1);
	if (!comb_payload) {
		perror("Error allocating payload memory");
		return -3;
	}
	memcpy(comb_payload, &head, sizeof(head));
	memcpy(comb_payload + sizeof(head), &report_err, sizeof(report_err));

	if (getpeername(sock_id, (struct sockaddr*)&peer_addr, &socklength) == 0) {
		write(error_report, comb_payload, payload_sz);
		printf("Error report: size(%lu)sent\n", payload_sz);
	}
	free(comb_payload);
	close(error_report);

	return 0;
}
