
#ifndef STRUCTS_H
 #define STRUCTS_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

extern const char *salt;

struct conn_args {
	int client_sock;
	pthread_mutex_t *lock;
	size_t *total_moles;
	size_t *total_chlor;
	size_t *total_air;
	size_t *total_lead;
};

struct molecule {
	uint32_t data;
	uint16_t left;
	uint16_t right;
};

struct hz_container {
	uint32_t data;
	uint32_t custom;
}; // sizeof(struct hz_container == 8

struct header {
	uint16_t type;
	uint16_t size;
	char custom[4];
};

struct hash {
	uint8_t code[64];
};

struct sludge {
	struct header *header;
	struct hash *hashes;
};

struct liquid {
	struct header *header;
	struct molecule *molecules;
};

bool serialize(const struct molecule *n, const size_t nmemb, char *buf, const size_t sz);

// N molecules will require N+1 0 values
// This will stop when a molecule is full; 0s at the end may be ommitted
struct liquid *deserialize(const char *buf, size_t *nmemb);

#endif
