
#include "structs.h"

#include <inttypes.h>
#include <stdio.h>

const char *salt = "I Hate Liam Echlin";

/*
static char *buf_ptr;
static size_t buf_sz;
void _serialize(uint32_t val) {
	int amt = snprintf(buf_ptr, buf_sz, "%" PRIu32 " ", val);
	buf_ptr += amt;
	buf_sz -= amt;
}

bool serialize(const struct molecule *t, char *buf, const size_t sz)
{
	if(!t || !buf) {
		return false;
	}

	buf_ptr = buf;
	buf_sz = sz;
	molecule_preorder(t, _serialize);

	return true;
}

void _deserialize(struct molecule **t)
{
	uint32_t value = 0;
	char *word = strtok(NULL, " ");
	if(!word) {
		return;
	}
	sscanf(word, "%" PRIu32, &value);
	if(value) {
		*t = molecule_create(value);
		_deserialize(&((*t)->left));
		_deserialize(&((*t)->rite));
	}
}

struct molecule *deserialize(const char *buf)
{
	if(!buf) {
		return false;
	}
	char *word = strtok((char *)buf, " ");

	uint32_t value = 0;
	sscanf(word, "%" PRIu32, &value);
	if(value) {
		struct molecule *molecule = molecule_create(value);
		_deserialize(&(molecule->left));
		_deserialize(&(molecule->rite));
		return molecule;
	}

	return NULL;
}

void molecule_preorder(const struct molecule *t,
		void (*func)(uint32_t))
{
	if(!t) {
		return;
	}

	func(t->data);
	molecule_preorder(t->left, func);
	molecule_preorder(t->rite, func);
}
*/

