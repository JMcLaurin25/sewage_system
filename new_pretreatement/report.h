#ifndef REPORT_H
#define REPORT_H

#include <stdint.h>

// RESERVED=0, not_enough_data_data=8, illegal_dumping=16
struct report {
	uint16_t error_type;
	uint16_t custom;
	int32_t ip_addr;
	char message[56];
};
// sizeof(struct report) == 64

int report(int sock_id, size_t err_type, const char *message, size_t msg_len);

#endif
